package me.Machu13.Guardian.Ban;

import me.Machu13.Guardian.Utils.CheatType;
import me.Machu13.Guardian.Utils.JsonMessages;
import org.bukkit.entity.Player;

public class GuardianMapper {
	public static void reportCheater(int caven_player_id, Player player, String serverName, CheatType cheat,
			int violation) {
		JsonMessages.sendJsonMessageAlert(caven_player_id, player, cheat, serverName, violation);
	}

	public static void reportTestCheater(int caven_player_id, Player player, String serverName, CheatType cheat,
			int violation) {
		JsonMessages.sendJsonTestMessageAlert(caven_player_id, player, cheat, serverName, violation);
	}
}
