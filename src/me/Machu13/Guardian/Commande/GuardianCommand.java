package me.Machu13.Guardian.Commande;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;
import me.Machu13.Guardian.Utils.CheatType;
import me.Machu13.Guardian.Utils.JsonMessages;

public class GuardianCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player))
			return true;

		Player player = (Player) sender;

		if (!player.isOp())
			return true;

		if (args.length > 0) {
			String subCommand = args[0];
			if (subCommand.equalsIgnoreCase("speed"))
				speed(player, args);
			else if (subCommand.equalsIgnoreCase("ff"))
				ff(player, args);
			else if (subCommand.equalsIgnoreCase("ban"))
				ban(player, args);
			else if (subCommand.equalsIgnoreCase("dmg"))
				dmg(player, args);
			else if (subCommand.equalsIgnoreCase("cheat"))
				cheat(player, args);
			else if (subCommand.equalsIgnoreCase("test"))
				test(player, args);
			else if (subCommand.equalsIgnoreCase("show"))
				show(player, args);
			else if (subCommand.equalsIgnoreCase("reset"))
				reset(player, args);
		}

		return true;
	}

	private void dmg(Player player, String[] args) {
		player.damage(10);
	}

	private void cheat(Player player, String[] args) {
		JsonMessages.sendJsonMessageAlert(0, player, CheatType.FASTREGEN, null, 0);
	}

	private void test(Player player, String[] args) {
		// for (String string : PlayerInfo.getModos()) {
		// Guardian.log(string);
		Guardian.log("" + player.getUniqueId());
		Guardian.log("" + Bukkit.getPlayerExact(player.getName()).getUniqueId());
		// player.sendMessage(JsonUUIDConverter.getPlayer(string));
		// }
		try {
			ByteArrayOutputStream b = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(b);
			out.writeUTF("Message");
			out.writeUTF(player.getName());
			out.writeUTF(ChatColor.RED + "Congrats, you just won 1$!");

			Bukkit.getServer().sendPluginMessage(Guardian.instance, "BungeeCord", b.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ban(Player player, String[] args) {
		GuardianPlayer guardianPlayer = Guardian.get(player);
		guardianPlayer.ban("test");
		player.sendMessage("Banned !");
	}

	private void reset(Player player, String[] args) {
		GuardianPlayer guardianPlayer = Guardian.get(player);
		player.sendMessage("Reset de stats");
		guardianPlayer.violationAntiKnockback = 0;
		guardianPlayer.violationFastFoodRegen = 0;
		guardianPlayer.violationForceField = 0;
		guardianPlayer.violationSpeedHack = 0;
	}

	private void show(Player player, String[] args) {
		GuardianPlayer guardianPlayer = Guardian.get(player);
		player.sendMessage("--- Stats cheat de " + player.getName() + "---");
		player.sendMessage("--- Grade: " + guardianPlayer.updateRank().getName() + " | Id: "
				+ guardianPlayer.updateCavenPlayerId() + " ---");
		player.sendMessage("FastRegen=" + guardianPlayer.violationFastFoodRegen);
		player.sendMessage("SpeedHack=" + guardianPlayer.violationSpeedHack);
		player.sendMessage("AntiKnockBack=" + guardianPlayer.violationAntiKnockback);
		player.sendMessage("ForceField=" + guardianPlayer.violationForceField);
	}

	private void ff(Player player, String[] args) {
		// ForceFieldChecker.check(player);
	}

	private void speed(Player player, String[] args) {
		if (args.length == 1) {
			player.sendMessage("WalkingSpeed actuel: �b" + player.getWalkSpeed());
			return;
		}
		if (args.length > 1) {
			float newSpeed = Float.parseFloat(args[1]);
			player.setWalkSpeed(newSpeed);
			player.sendMessage("Nouvelle WalkingSpeed: �b" + player.getWalkSpeed());
			return;
		}
	}

}
