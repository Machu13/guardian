package me.Machu13.Guardian;

import org.bukkit.configuration.Configuration;

public class GuardianConfig {

	/*
	 * D�clancheur de report
	 */
	public static int reportForceFieldViolation = 80;
	public static int reportSpeedHackViolation = 100;
	public static int reportKnockBackViolation = 20;
	public static int reportFastFoodRegenViolation = 8000;

	/*
	 * Configuration
	 */
	// G�n�rale
	public static boolean pluginEnabled = true;
	// Move
	public static boolean forcedSprint = false;

	/**
	 * Chargement de la configuration
	 */
	public static void load() {
		Guardian.instance.saveDefaultConfig();
		Configuration config = Guardian.instance.getConfig();
		pluginEnabled = config.getBoolean("enabled");
		Guardian.log("Enabled = " + pluginEnabled);
		// Move
		forcedSprint = config.getBoolean("move.forcedSprint");
		Guardian.log("ForcedSprint = " + forcedSprint);
	}

}
