package me.Machu13.Guardian.Move;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;

public class SpeedListenerRunnable implements Runnable {

	public static long tickRate = 20;

	Player player;
	String playerName;
	GuardianPlayer guardianPlayer;
	BukkitTask task;
	MoveInfo move;

	double hSpeed;
	double vSpeed;
	double speed;

	public SpeedListenerRunnable(Player player) {
		this.player = player;
		this.playerName = player.getName();
		this.guardianPlayer = Guardian.get(player);
		this.task = Bukkit.getScheduler().runTaskTimer(Guardian.instance, this, 0, tickRate);
	}

	@Override
	public void run() {
		// Joueur est en ligne
		if (!player.isOnline())
			cancel();

		this.move = new MoveInfo(player, guardianPlayer.lastLocation);
		guardianPlayer.resetTicksAndModifiers();
		// Le joueur a boug�
		if (move.hasMoved) {
			// Save loc
			guardianPlayer.lastLocation = player.getLocation();

			// Bypass
			if (guardianPlayer.haveBypass(player))
				return;

			if (move.horizontalSpeedHacking)
				guardianPlayer.updateViolationSpeedHack(move.speed);
			
			
			/*if (move.horizontalSpeed > hSpeed)
				hSpeed = move.horizontalSpeed;
			if (move.verticalSpeed > vSpeed)
				vSpeed = move.verticalSpeed;
			if (move.speed > speed)
				speed = move.speed;

			String h = " �RH-�e" + String.format("%.2f", move.horizontalSpeed) + ":�c" + String.format("%.2f", hSpeed);
			String v = " �RV-�e" + String.format("%.2f", move.verticalSpeed) + ":�c" + String.format("%.2f", vSpeed);
			String s = " �RS-�e" + String.format("%.2f", move.speed) + ":�c" + String.format("%.2f", speed);

			Guardian.log(h + v + s);
			*/

		}
	}

	private void cancel() {
		Guardian.log("Arr�t du SpeedListener pour " + playerName);
		task.cancel();
	}

}
