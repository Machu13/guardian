package me.Machu13.Guardian.Move;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class MoveInfo {
	static double basePlayerWalkingSpeed = 0.2D;
	static double baseSneakSpeed = 0.12D;
	static double baseWalkSpeed = 0.35D;
	static double baseSprintSpeed = 0.45D;
	static double baseSprintJumpSpeedBonus = 0.18D;
	Location from;
	Location to;
	public boolean hasMoved;
	public boolean changeWorld;
	public double x;
	public double y;
	public double z;
	public double horizontalSpeed;
	public double verticalSpeed;
	public double speed;
	public boolean horizontalSpeedHacking;
	GuardianPlayer guardianPlayer;

	public MoveInfo(Player player, Location from) {
		this.guardianPlayer = Guardian.get(player);
		this.from = from;
		this.to = player.getLocation();
		this.changeWorld = (!from.getWorld().equals(this.to.getWorld()));
		this.x = Math.abs(from.getX() - this.to.getX());
		this.y = Math.abs(from.getY() - this.to.getY());
		this.z = Math.abs(from.getZ() - this.to.getZ());

		this.horizontalSpeed = (this.x + this.z);
		this.verticalSpeed = this.y;
		this.speed = (this.horizontalSpeed + this.verticalSpeed);

		this.hasMoved = ((this.changeWorld) || (this.x > 0.0D) || (this.y > 0.0D) || (this.z > 0.0D));

		double maxMoveSpeed = player.getWalkSpeed() * getPotionBoost(player) * getBaseHSpeedOnTicks()
				/ basePlayerWalkingSpeed;
		double latencyModifier = 1 + Guardian.get(player).getPing() / 10000;
		double maxHSpeed = maxMoveSpeed * latencyModifier;
		if (this.horizontalSpeed > maxHSpeed) {
			this.horizontalSpeedHacking = true;
		}
	}

	public double getPotionBoost(Player player) {
		double result = 1.0D;
		for (PotionEffect effect : player.getActivePotionEffects()) {
			if (effect.getType().equals(PotionEffectType.SPEED)) {
				result += 0.2D * (effect.getAmplifier() + 1);
			} else if (effect.getType().equals(PotionEffectType.SLOW)) {
				result += 0.15D * (effect.getAmplifier() + 1);
			}
		}
		return result;
	}

	public double getBaseHSpeedOnTicks() {
		double sneakMaxSpeed = this.guardianPlayer.sneakTicks * baseSneakSpeed;
		double walkMaxSpeed = this.guardianPlayer.walkTicks * baseWalkSpeed;
		double sprintMaxSpeed = this.guardianPlayer.sprintTicks * baseSprintSpeed;
		double jumpMaxSpeed = this.guardianPlayer.jumpTicks * baseSprintJumpSpeedBonus;
		return sneakMaxSpeed + walkMaxSpeed + sprintMaxSpeed + jumpMaxSpeed;
	}
}
