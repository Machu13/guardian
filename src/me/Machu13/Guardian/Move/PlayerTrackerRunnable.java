package me.Machu13.Guardian.Move;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class PlayerTrackerRunnable implements Runnable {
	public static long tickRate = 1L;
	Player player;
	String playerName;
	GuardianPlayer guardianPlayer;
	BukkitTask task;

	public PlayerTrackerRunnable(Player player) {
		this.player = player;
		this.playerName = player.getName();
		this.guardianPlayer = Guardian.get(player);
		this.guardianPlayer.lastLocation = player.getLocation();

		this.task = Bukkit.getScheduler().runTaskTimer(Guardian.instance, this, 1L, tickRate);
	}

	public void run() {
		if (!this.player.isOnline()) {
			cancel();
		}
		this.guardianPlayer.currentLocation = this.player.getLocation();
		if (this.player.isSneaking()) {
			this.guardianPlayer.updateSneak();
		} else if (this.player.isSprinting()) {
			this.guardianPlayer.updateSprint();
		} else {
			this.guardianPlayer.updateIsWalking();
		}
		if (!this.guardianPlayer.updateIsOnGround()) {
			this.guardianPlayer.updateJumping();
		}
	}

	private void cancel() {
		Guardian.log("Arr�t du Tracker pour " + this.playerName);
		this.task.cancel();
	}
}
