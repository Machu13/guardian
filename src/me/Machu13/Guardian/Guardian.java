package me.Machu13.Guardian;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import me.Machu13.Guardian.Listeners.PlayerConnectEvent;
import me.Machu13.Guardian.Move.SpeedListenerRunnable;

public class Guardian extends JavaPlugin {

	public static Plugin instance;
	private static Map<String, GuardianPlayer> guardianPlayers = new HashMap<>();
	private static List<BukkitTask> runningTasks = new ArrayList<>();

	@Override
	public void onEnable() {
		instance = this;
		PluginManager pm = getServer().getPluginManager();
		// Mouvement
		pm.registerEvents(new PlayerConnectEvent(), this);

		// Reload
		reload();
	}

	@Override
	public void onDisable() {
		sendStats();
	}

	/**
	 * Renvois les statistiques de cheats du joueur en DB
	 */
	private void sendStats() {

	}

	public static void log(String string) {
		Bukkit.getConsoleSender().sendMessage(string);
	}

	/**
	 * Reload du plugin ou autre
	 */
	public static void reload() {
		for (BukkitTask task : runningTasks) {
			task.cancel();
		}
		for (Player player : Bukkit.getOnlinePlayers()) {
			initPlayer(player);
		}
	}

	/**
	 * Recupere un guardianPlayer
	 * 
	 * @param player
	 * @return
	 */
	public static GuardianPlayer get(Player player) {
		return guardianPlayers.get(player.getName());
	}

	/**
	 * Initialisation sur le joueur
	 * 
	 * @param player
	 * @return
	 */
	public static void initPlayer(Player player) {
		if (get(player) == null)
			guardianPlayers.put(player.getName(), new GuardianPlayer(player));
		new SpeedListenerRunnable(player);
	}

}
