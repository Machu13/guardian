package me.Machu13.Guardian.Utils;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireWorkUtils {
	public static void createFireworkEffect(Location spawn, Color randomColor) {
		Firework fw = (Firework) spawn.getWorld().spawn(spawn, Firework.class);
		FireworkMeta fm = fw.getFireworkMeta();
		fm.setPower(0);
		fm.addEffects(new FireworkEffect[] { FireworkEffect.builder().trail(true).with(FireworkEffect.Type.BALL)
				.withColor(randomColor).withFade(Color.OLIVE).build() });
		fw.setFireworkMeta(fm);

		Firework fw2 = (Firework) spawn.getWorld().spawn(spawn, Firework.class);
		FireworkMeta fm2 = fw2.getFireworkMeta();
		fm2.setPower(0);
		fm2.addEffects(new FireworkEffect[] { FireworkEffect.builder().trail(true).with(FireworkEffect.Type.BURST)
				.withColor(randomColor).withFade(Color.OLIVE).build() });
		fw2.setFireworkMeta(fm2);

		Firework fw3 = (Firework) spawn.getWorld().spawn(spawn, Firework.class);
		FireworkMeta fm3 = fw3.getFireworkMeta();
		fm3.setPower(0);
		fm3.addEffects(new FireworkEffect[] { FireworkEffect.builder().trail(true).with(FireworkEffect.Type.STAR)
				.withColor(randomColor).withFade(Color.OLIVE).build() });
		fw3.setFireworkMeta(fm3);
	}

	public static void createGuardianFireworkEffect(Location spawn) {
		Firework firework = (Firework) spawn.getWorld().spawn(spawn, Firework.class);
		FireworkMeta fireworkMeta = firework.getFireworkMeta();
		FireworkEffect effect = FireworkEffect.builder().flicker(false).trail(false)
				.with(FireworkEffect.Type.BALL_LARGE).withColor(Color.RED).withFade(Color.OLIVE).build();
		FireworkEffect effect1 = FireworkEffect.builder().flicker(false).trail(false).with(FireworkEffect.Type.BURST)
				.withColor(Color.FUCHSIA).withFade(Color.OLIVE).build();
		fireworkMeta.addEffect(effect);
		fireworkMeta.addEffect(effect1);
		firework.setFireworkMeta(fireworkMeta);
	}
}
