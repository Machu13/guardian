package me.Machu13.Guardian.Utils;

public enum CheatType {

	FORCEFIELD("ForceField"), FASTREGEN("FastFoodRegen"), ANTIKNOCKBACK("AntiKnockback"), SPEEDHACK("SpeedHack");

	String name;

	private CheatType(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
