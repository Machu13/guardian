package me.Machu13.Guardian.Utils;

public enum RankType {
	ADMIN("Admin", Integer.valueOf(8)), GERANT("Gerant", Integer.valueOf(7)), MODO("Modo",
			Integer.valueOf(6)), GUIDE("Guide", Integer.valueOf(5)), DONATEUR("Donateur",
					Integer.valueOf(4)), YTB("Youtubeur", Integer.valueOf(3)), MINIVIP("MiniVip",
							Integer.valueOf(2)), CHAMPIONS("Champions", Integer.valueOf(1)), JOUEUR("Joueur",
									Integer.valueOf(0)), DEBUG("Debug", Integer.valueOf(999));

	String name;
	Integer id;

	private RankType(String name, Integer id) {
		this.name = name;
		this.id = id;
	}

	public Integer getRankId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String toString() {
		return this.name;
	}
}
