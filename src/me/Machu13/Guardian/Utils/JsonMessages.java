package me.Machu13.Guardian.Utils;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class JsonMessages {

	public JsonMessages(String msg) {
	}

	public static void sendJsonMessageAlert(int caven_player_id, Player player, CheatType cheat, String serveur,
			int violation) {
		GuardianPlayer guardianPlayer = Guardian.get(player);
		IChatBaseComponent comp = ChatSerializer
				.a("{\"text\":\"\",\"extra\":[{\"text\":\"(\",\"color\":\"gray\"},{\"text\":\"Guardian\",\"color\":\"red\"},{\"text\":\") \",\"color\":\"gray\"},{\"text\":\""
						+ serveur + ":  \",\"color\":\"yellow\"},{\"text\":\"" + player.getName() + " > "
						+ cheat.getName()
						+ " \",\"color\":\"green\"},{\"text\":\"[x]\",\"color\":\"dark_red\",\"bold\":true,\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/server "
						+ serveur
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Player: \",\"color\":\"aqua\"},{\"text\":\""
						+ player.getName()
						+ "\n\",\"color\":\"light_purple\"},{\"text\":\"Ping: \",\"color\":\"aqua\"},{\"text\":\""
						+ guardianPlayer.getPing()
						+ " ms\n\",\"color\":\"light_purple\"},{\"text\":\"Serveur: \",\"color\":\"aqua\"},{\"text\":\""
						+ serveur
						+ "\n\",\"color\":\"light_purple\"},{\"text\":\"CheatLevel: \",\"color\":\"aqua\"},{\"text\":\""
						+ guardianPlayer.cheatLevel + "\",\"color\":\"light_purple\"}]}}}]}");
		PacketPlayOutChat packet = new PacketPlayOutChat(comp);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}

	public static void sendJsonTestMessageAlert(int caven_player_id, Player player, CheatType cheat, String serveur,
			int violation) {
		GuardianPlayer guardianPlayer = Guardian.get(player);
		IChatBaseComponent comp = ChatSerializer
				.a("{\"text\":\"\",\"extra\":[{\"text\":\"(\",\"color\":\"gray\"},{\"text\":\"Guardian\",\"color\":\"red\"},{\"text\":\") \",\"color\":\"gray\"},{\"text\":\""
						+ serveur + ":  \",\"color\":\"yellow\"},{\"text\":\"" + player.getName() + " > "
						+ cheat.getName()
						+ " \",\"color\":\"green\"},{\"text\":\"[x]\",\"color\":\"dark_red\",\"bold\":true,\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/server "
						+ serveur
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Player: \",\"color\":\"aqua\"},{\"text\":\""
						+ player.getName()
						+ "\n\",\"color\":\"light_purple\"},{\"text\":\"Ping: \",\"color\":\"aqua\"},{\"text\":\""
						+ guardianPlayer.getPing()
						+ " ms\n\",\"color\":\"light_purple\"},{\"text\":\"Serveur: \",\"color\":\"aqua\"},{\"text\":\""
						+ serveur
						+ "\n\",\"color\":\"light_purple\"},{\"text\":\"CheatLevel: \",\"color\":\"aqua\"},{\"text\":\""
						+ guardianPlayer.cheatLevel + "\",\"color\":\"light_purple\"}]}}}]}");
		PacketPlayOutChat packet = new PacketPlayOutChat(comp);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
}
