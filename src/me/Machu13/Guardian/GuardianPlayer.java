package me.Machu13.Guardian;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import me.Machu13.Guardian.Ban.GuardianMapper;
import me.Machu13.Guardian.Ban.PlayerMapper;
import me.Machu13.Guardian.Utils.CheatType;
import me.Machu13.Guardian.Utils.FireWorkUtils;
import me.Machu13.Guardian.Utils.RankType;

public class GuardianPlayer {

	public String name;
	public PlayerInfo info;
	public int caven_player_id;
	public RankType rank;
	public int cheatLevel;
	public Location lastLocation;
	public Location currentLocation;

	// Ban
	public boolean banned = false;

	// Status
	public boolean isOnGround = true;
	public boolean isSneak = false;
	public boolean isSprinting = false;
	public boolean isJumping = false;

	// Points de cheat
	public double violationSpeedHack = 0;
	public double violationAntiKnockback = 0;
	public int violationFastFoodRegen = 0;
	public int violationForceField = 0;

	// Stockage des Ticks du joueur
	public int walkTicks;
	public int sprintTicks;
	public int jumpTicks;
	public int sneakTicks;

	// Regen
	public long lastFoodRegen;

	// Attacks
	public int entityAttacks;

	// Velocity
	public double velocityGraceSeconds;

	// Bypass
	public boolean justTeleported = false;
	public boolean justRespawned = false;
	public boolean justDied = false;
	public boolean justHurted = false;

	public GuardianPlayer(Player player) {
		this.name = player.getName();
		this.info = PlayerInfo.get(player);
		this.caven_player_id = info.getId();
		this.rank = info.getRank();
		this.currentLocation = player.getLocation();
		this.lastLocation = currentLocation;
	}

	// Total move ticks
	public int getTotalMoveTicks() {
		return walkTicks + sprintTicks + sneakTicks;
	}

	// Get ping
	public int getPing() {
		Player player = Bukkit.getPlayer(name);
		if (player == null)
			return 0;
		if (!player.isValid())
			return 0;
		if (!player.isOnline())
			return 0;
		CraftPlayer cp = (CraftPlayer) player;
		return Integer.valueOf(cp.getHandle().ping);
	}

	// Joueur au sol
	public boolean updateIsOnGround() {
		Block block = currentLocation.getBlock().getRelative(0, -1, 0);
		// Block solid dessous ?
		if (block.getType().isSolid())
			isOnGround = true;
		// Carr� autour du bloc actuel si sneak
		else if (isSneak) {
			for (int x = -1; x <= 1; x++) {
				for (int z = -1; z <= 1; z++) {
					if (x == 0 && z == 0)
						continue;
					else if (block.getRelative(x, 0, z).getType().isSolid()) {
						isOnGround = true;
						break;
					}
				}
				isOnGround = false;
			}
		} else
			isOnGround = false;
		return isOnGround;
	}

	// Reset de tous les ticks
	public void resetTicksAndModifiers() {
		walkTicks = 0;
		sprintTicks = 0;
		jumpTicks = 0;
		sneakTicks = 0;
	}

	// Joueur sneak
	public void updateSneak() {
		sneakTicks++;
		isSneak = true;
		isSprinting = false;
	}

	// Joueur sprint
	public void updateSprint() {
		sprintTicks++;
		isSneak = false;
		isSprinting = true;
	}

	// Joueur marche
	public void updateIsWalking() {
		walkTicks++;
		isSneak = false;
		isSprinting = false;
	}

	// Joueur saute
	public void updateJumping() {
		jumpTicks++;
		isJumping = true;
	}

	// A une raison de d�passer les valeurs de speedHack
	public boolean haveBypass(Player player) {
		boolean result = false;
		// Peut voler
		if (player.getAllowFlight())
			result = true;
		// Est mort
		if (player.isDead())
			result = true;
		// Est dans un v�hicule
		if (player.getVehicle() != null)
			result = true;
		// Viens de se faire tp
		if (justTeleported) {
			justTeleported = false;
			result = true;
		}
		// Viens de respawn
		if (justRespawned) {
			justRespawned = false;
			result = true;
		}
		// Viens de mourir
		if (justDied) {
			justDied = false;
			result = true;
		}
		return result;
	}

	/*
	 * VIOLATION
	 */

	/**
	 * Violation FastRegen
	 * 
	 * @param violation
	 * @return
	 */
	public void updateViolationFastRegen(long violation) {
		violationFastFoodRegen += violation;
		if (violationFastFoodRegen >= GuardianConfig.reportFastFoodRegenViolation) {
			reportTest(CheatType.FASTREGEN, violationFastFoodRegen);
			ban("FastHeathRegen");
			violationFastFoodRegen = 0;
		}

	}

	/**
	 * Violation SpeedHack
	 * 
	 * @param value
	 * @return
	 */
	public boolean updateViolationForceField() {
		violationForceField++;
		if (violationForceField >= GuardianConfig.reportForceFieldViolation) {
			ban("ForceField");
			violationForceField = 0;
			return true;
		}
		return false;
	}

	/**
	 * Violation SpeedHack
	 * 
	 * @param value
	 * @return
	 */
	public boolean updateViolationSpeedHack(double value) {
		violationSpeedHack += value;
		if (violationSpeedHack >= GuardianConfig.reportSpeedHackViolation) {
			reportTest(CheatType.SPEEDHACK, (int) violationSpeedHack);
			violationSpeedHack = 0;
			return true;
		}
		return false;
	}

	/**
	 * Violation AntiKnockBack
	 * 
	 * @return
	 */
	public boolean updateViolationAntiKnockback() {
		violationAntiKnockback++;
		if (violationAntiKnockback >= GuardianConfig.reportKnockBackViolation) {
			reportTest(CheatType.ANTIKNOCKBACK, (int) violationAntiKnockback);
			violationAntiKnockback = 0;
			return true;
		}
		return false;
	}

	/**
	 * Update le rang du joueur
	 * 
	 * @return
	 */
	public RankType updateRank() {
		this.rank = info.getRank();
		return this.rank;
	}

	/**
	 * Update l'id du CavenPlayer
	 * 
	 * @return
	 */
	public Integer updateCavenPlayerId() {
		this.caven_player_id = info.getId();
		return this.caven_player_id;
	}

	/**
	 * Report standAlone
	 * 
	 * @param cheat
	 * @param violation
	 */
	@SuppressWarnings("unused")
	private void report(CheatType cheat, int violation) {
		GuardianMapper.reportCheater(caven_player_id, Bukkit.getPlayer(name), Bukkit.getServerName(), cheat, violation);
	}

	/**
	 * Report test
	 * 
	 * @param cheat
	 * @param violation
	 */
	private void reportTest(CheatType cheat, int violation) {
		GuardianMapper.reportTestCheater(caven_player_id, Bukkit.getPlayer(name), Bukkit.getServerName(), cheat,
				violation);
	}

	/**
	 * Ban du joueur
	 * 
	 * @param reason
	 */
	public void ban(String reason) {
		// TODO//
		if (banned)
			return;
		// this.banned = true;
		// Ban en DB
		PlayerMapper.guardianBan(name, reason);
		Player player = Bukkit.getPlayer(name);
		// Annimation
		if (player != null) {
			final Location spawn = player.getEyeLocation();
			player.getWorld().strikeLightningEffect(spawn);
			player.getWorld().playSound(spawn, Sound.ENDERDRAGON_GROWL, 3.0F, 1.0F);
			player.getWorld().playSound(spawn, Sound.WOLF_GROWL, 1.0F, 1.0F);
			for (int x = 0; x < 5; x++) {
				Bukkit.getScheduler().runTaskLater(Guardian.instance, new Runnable() {
					@Override
					public void run() {
						spawn.add(Vector.getRandom());
						FireWorkUtils.createGuardianFireworkEffect(spawn);
					}
				}, 3 * x + 1);
			}
		}
		// Annonce + disconnect
		// if (!reason.equals("test")) BungeeEventSender.wardenBan(name,
		// reason);
	}

	/**
	 * Diminue un peu les violations dans le temps (lag/faux positif)
	 */
	public void descreaseViolation() {
		if (violationForceField > 0)
			violationForceField--;
		if (violationAntiKnockback > 0)
			violationAntiKnockback -= 5;
		if (violationSpeedHack > 0)
			violationSpeedHack -= 5;
		if (violationFastFoodRegen > 0)
			violationFastFoodRegen -= 30;
	}
}
