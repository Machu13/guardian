package me.Machu13.Guardian.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;

public class PlayerDamageEvent implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayerDamageEvent(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if (event.getCause() == DamageCause.FALL) {
				GuardianPlayer guardianPlayer = Guardian.get(player);
				guardianPlayer.justHurted = true;
			}
		}
	}

}
