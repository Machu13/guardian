package me.Machu13.Guardian.Listeners;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;

public class PlayerHealthEvent implements Listener {
	static long vanillaFoodRegen = 3600L;

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void onPlayerRegainHealth(EntityRegainHealthEvent event) {
		if (((event.getEntity() instanceof Player))
				&& (event.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED)) {
			Player player = (Player) event.getEntity();
			GuardianPlayer guardianPlayer = Guardian.get(player);
			long lastRegenTime = System.currentTimeMillis() - guardianPlayer.lastFoodRegen;
			guardianPlayer.lastFoodRegen = System.currentTimeMillis();
			if (lastRegenTime < vanillaFoodRegen) {
				guardianPlayer.updateViolationFastRegen(vanillaFoodRegen);
			}
		}
	}
}
