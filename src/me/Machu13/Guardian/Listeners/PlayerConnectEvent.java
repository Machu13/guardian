package me.Machu13.Guardian.Listeners;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.mojang.util.UUIDTypeAdapter;

import me.Machu13.Guardian.Guardian;

public class PlayerConnectEvent implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		try {
			Guardian.log(player.getUniqueId().toString());
		} catch (Exception e) {
		}
		try {
			Guardian.log("UUID - Name = " + UUID.fromString(player.getName()));
		} catch (Exception e) {
		}
		try {
			Guardian.log("UUID - UUID = " + UUID.fromString(player.getUniqueId().toString()));
		} catch (Exception e) {
		}
		try {
			Guardian.log("MojangString - Name = " + UUIDTypeAdapter.fromString(player.getName()));
		} catch (Exception e) {
		}

		try {
			Guardian.log("MojangString - UUID = " + UUIDTypeAdapter.fromString(player.getUniqueId().toString()));
		} catch (Exception e) {
		}

		try {
			Guardian.log("MojangUUID - UUID = " + UUIDTypeAdapter.fromUUID(player.getUniqueId()));
		} catch (Exception e) {
		}

		Guardian.initPlayer(player);
	}

}
