package me.Machu13.Guardian.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;

public class PlayerDeathListener implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayeDie(PlayerDeathEvent event) {
		Player player = event.getEntity();
		GuardianPlayer guardianPlayer = Guardian.get(player);
		guardianPlayer.justDied = true;
	}

}
