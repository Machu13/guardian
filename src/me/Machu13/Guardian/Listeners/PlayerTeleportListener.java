package me.Machu13.Guardian.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

import me.Machu13.Guardian.Guardian;
import me.Machu13.Guardian.GuardianPlayer;

public class PlayerTeleportListener implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		GuardianPlayer guardianPlayer = Guardian.get(player);
		guardianPlayer.justTeleported = true;
	}

}
